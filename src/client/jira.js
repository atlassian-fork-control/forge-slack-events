import { error, debug } from '../util/logger';

export async function fetchIssue(issueIdOrKey, fields) {
  let fieldsQuery = '';
  if (fields && fields.length > 0) {
    fieldsQuery = `?fields=${fields.join(',')}`;
  }
  return doGet(`/rest/api/3/issue/${issueIdOrKey}${fieldsQuery}`);
}

export async function fetchUser(accountId) {
  return doGet(`/rest/api/3/user?accountId=${accountId}`);
}

async function doGet(restUrl) {
  const response = await api.asApp().requestJira(restUrl);
  if (!response.ok) {
    const err = `Error invoking ${restUrl} (Jira): ${response.status} ${response.statusText}`;
    error(err);
    throw new Error(err);
  }
  const responseBody = await response.json();
  debug(`Response from Jira: ${JSON.stringify(responseBody)}`);
  return responseBody;
}
